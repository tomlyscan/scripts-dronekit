from dronekit import connect, VehicleMode, LocationGlobalRelative, LocationGlobal, Command
import time
import math
from pymavlink import mavutil

# Por Thyago Oliveira, graduando de Engenharia de Computacao - UFPB
# Nota: Pelo fato da versao o python a ser utilizada eh a versao pre-3.0, evite usar acentos e outros
#    caracteres especiais, inclusive nos comentarios, sob risco de resultar em erros de interpretacao.
#**************************************************************************************************
def arm_and_takeoff(aTargetAltitude,vehicle):
    """
    Funcao extraida do exemplo "simple_goto.py"
    Documentacao: http://python.dronekit.io/examples/simple_goto.html
    Funcao que arma o veiculo e o faz voar ate a altitude aTargetAltitude
    Entradas:
        aTargetAltitude: integer (altitude a ser alcancada pelo veiculo)
        vehicle: Vehicle (veiculo conectado)
    Estado do metodo: pronto, aguardando testes
    """
    #Checagem basica pre-armamento
    while not vehicle.is_armable:
        sleep(1)
        # Aguarde enquanto o veiculo ainda nao esta armado

    #Armando motores -- O drone arma em modo GUIDED
    vehicle.mode = VehicleMode("GUIDED")
    vehicle.armed = True

    #Confirma se o drone esta armado antes do comando take off
    while not vehicle.armed:
        time.sleep(1);

    #Levanta voo
    vehicle.simple_takeoff(aTargetAltitude)

    #Aguarda o drone ficar em uma altitude estavel antes que um novo comando possa ser executado
    while True:
        if vehicle.location.global_relative_frame.alt>=aTargetAltitude*0.95:
            break
        time.sleep(1)
#**************************************************************************************************


def send_global_velocity(velocity_x, velocity_y, velocity_z, duration, vehicle):
    msg = vehicle.message_factory.set_position_target_global_int_encode(
        0,
        0, 0,
        mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT,
        0b0000111111111000,
        aLocation.lat*1e7,
        aLocation.lon*1e7,
        aLocation.alt,)


def move(direction, duration, speed):
    """
    Funcao que move um drone em uma determinada direcao por um periodo de tempo
    Entradas:
        direction: string (left,right,up,down,back,foward)
        duration: integer (tempo em segundos)
        speed: float (velocidade em km/h)
    """
    if(direction == "left"):




