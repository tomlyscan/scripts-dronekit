from dronekit import connect, VehicleMode, LocationGlobalRelative, LocationGlobal, Command
import time
import math
from pymavlink import mavutil

identificador = 0
class Status:
	def __init__(self, velocidade, energia, posicao):
		self.velocidade = velocidade
		self.energia = energia
		self.posicao = posicao

class Bateria:
	def __init__(self, tensao, corrente, nivel):
		self.tensao = None
		self.corrente = None
		self.nivel = nivel

#Configurando o parser para gerar a string de conexao
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--connect',
		     default='127.0.0.1:14550',
                     help="vehicle connection target. Default '127.0.0.1:14550'")
args = parser.parse_args()

#Conectando-se com o veiculo
vehicle = connect('127.0.0.1:14550',
                  wait_ready=True)

def arm_and_takeoff(aTargetAltitude):
	while not vehicle.is_armable:
		time.sleep(1)

	vehicle.mode	= VehicleMode("GUIDED")
	vehicle.armed	= True

	while not vehicle.armed:
		time.sleep(1)

	vehicle.simple_takeoff(aTargetAltitude)

	while True:
		if vehicle.location.global_relative_frame.alt >= aTargetAltitude*0.95:
			break
		time.sleep(1)

def battery_estimate(offset):
	return (vehicle.battery.level - offset)

def init(posicao, energia):
    '''
	Descricao: Funcao que inicializa um drone dado a sua posicao inicial e a energia restante.

	Utilizacao:
		posicao: variavel que guarda a latitude e a longitude para a inicializacao.
			 exemplo: posicao = LocationGlobalRelative(latitude, longitude, altitude)
                         A latitude e longitude sao relativos ao sistema de coordenadas WGS84.

		energia: variavel que configura os dados referentes a bateria
			 inteiro que recebe o valor da bateria atual.

    '''
    from dronekit_sitl import SITL
    sitl = SITL()
    sitl.download('copter', '3.3', verbose=True)
    if ((posicao.lat is not None and posicao.lon is None) or
	(posicao.lat is None and posicao.lon is not None)):
	print "Insira valores validos para altitude e longitude"
	exit(1)
    sitl_args = ['-IO', '--model', 'quad', ]
    if posicao.lat is not None:
	sitl_args.append('--home=%f,%f,40,299' % (posicao.lat, posicao.lon, ))
    sitl.launch(sitl_args, await_ready=True, restart=True)
    vehicle = connect('127.0.0.1:14550',wait_ready=True)

    return (sitl, vehicle)

def action(action,duration,velocity,identifier):
	velocity_x = velocity
	velocity_y = velocity
	velocity_z = velocity
	if action == 'N' or action == 'n':		# Mover o drone para a direcao NORTE
		velocity_y = 0
		velocity_z = 0
	elif action == 'S' or action == 's':	# Mover o drone para a direcao SUL
		velocity_x = velocity_x*(-1)
		velocity_y = 0
		velocity_z = 0
	elif action == 'W' or action == 'w':	# Mover o drone para a direcao OESTE
		velocity_x = 0
		velocity_y = velocity_y*(-1)
		velocity_z = 0
	elif action == 'E' or action == 'e':	# Mover o drone para a direcao LESTE
		velocity_x = 0
		velocity_z = 0
	elif action == 'T' or action == 't':	# Mover o drone para cima (TOP)
		velocity_x = 0
		velocity_y = 0
	elif action == 'B' or action == 'b':	# Mover o drone para baixo (BOTTON)
		velocity_x = 0
		velocity_y = 0
		velocity_z = velocity_z*(-1)

	else:
		print "Digite um caracter valido para a acao"
		exit(1)

	msg = vehicle.message_factory.set_position_target_local_ned_encode(
		0,                                      # time_boot_ms
		0,0,                                    # target system, target component
		mavutil.mavlink.MAV_FRAME_LOCAL_NED,    # frame
		0b0000111111000111,                     # type_mask (apenas com a velocidade habilitada)
		0,0,0,                                  # posicoes no eixo cartesiano (x, y, z)
		velocity_x,velocity_y,velocity_z,       # velocidade em m/s com relacao aos eixos
		0,0,0,                                  # aceleracao em relacao aos eixos (nao suportado, ignorado em GCS_Mavlink)
		0,0)                                    # yaw, yaw_rate (nao suportado, ignorado em GCS_Mavlink)

	for x in range(0,duration):
		vehicle.send_mavlink(msg)
		#print "Battery estimate: " + str(battery_estimate(offset))
		print "################################### FRAME " + str(x) + " ###################################\n"
		print "Vehicle ID: " + str(identifier) + '\n'
		print "Velocidade eixo x: " + str(vehicle.velocity[0]) + '\n'
		print "Velocidade eixo y: " + str(vehicle.velocity[1]) + '\n'
		print "Velocidade eixo z: " + str(vehicle.velocity[2]) + '\n'
		print "Estimativa da bateria: " + str(vehicle.battery.level) + '\n'
		print str(vehicle.location.local_frame) + '\n'
		print "################################################################################"

		time.sleep(1)

def getState():
	return (vehicle.velocity[0],
			vehicle.velocity[1],
			vehicle.velocity[2],
			vehicle.battery.level,
			vehicle.location.local_frame,
			identificador)

# Testes:
#TESTPOINT = LocationGlobalRelative(-7.163147, -34.818550, 2)

#(sitl, drone) = init(TESTPOINT, 50)

arm_and_takeoff(20)

action('n',20,40,1)
action('e',20,40,1)

#time.sleep(10)

drone.close()
