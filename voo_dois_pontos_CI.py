from dronekit import connect, VehicleMode, LocationGlobalRelative, LocationGlobal, Command
import time
import math
from pymavlink import mavutil


#Configurando o parser para gerar a string para a conexao
import argparse
parser = argparse.ArgumentParser(description='Demonstracao de voo de um drone simulado em dois pontos no CI.')
parser.add_argument('--connect', default='127.0.0.1:14550',
                   help="vehicle connection target. Default '127.0.0.1:14550'")
args = parser.parse_args()

#Conectando-se com o veiculo
print 'Conectando-se ao veiculo em: %s' % args.connect
vehicle = connect('127.0.0.1:14550', wait_ready=True)

def arm_and_takeoff(aTargetAltitude):
    """
    Arms vehicle and fly to aTargetAltitude.
    """

    print "Checagem basica pre-armamento... "
    # Don't try to arm until autopilot is ready
    while not vehicle.is_armable:
        print " Aguardando inicializacao do veiculo..."
        time.sleep(1)


    print "Armando motores"
    # Copter should arm in GUIDED mode
    vehicle.mode    = VehicleMode("GUIDED")
    vehicle.armed   = True

    # Confirm vehicle armed before attempting to take off
    while not vehicle.armed:
        print " Aguardando para armar..."
        time.sleep(1)

    print "Decolando!"
    vehicle.simple_takeoff(aTargetAltitude) # Take off to target altitude

    # Wait until the vehicle reaches a safe height before processing the goto (otherwise the command
    #  after Vehicle.simple_takeoff will execute immediately).
    while True:
        print " Altitude: ", vehicle.location.global_relative_frame.alt
        #Break and return from function just below target altitude.
        if vehicle.location.global_relative_frame.alt>=aTargetAltitude*0.95:
            print "Chegou a altitude alvo."
            break
        time.sleep(1)

arm_and_takeoff(2)

print "Mudando airspeed padrao para 23m/s"
vehicle.airspeed=23

point1 = LocationGlobalRelative(-7.163147,-34.818550,2)
point2 = LocationGlobalRelative(-7.162785,-34.816897,2)
point3 = LocationGlobalRelative(-7.163573,-34.816799,1)

vehicle.simple_goto(point1,groundspeed=30)
#vehicle.simple_goto(point1,airspeed=10,airspeed=5)

contador = 0
while vehicle.battery.level >= 30:
    print "Nivel da bateria: ", vehicle.battery.level
    #if contador % 10 == 0:
    if vehicle.battery.level >= 80:
        vehicle.simple_goto(point2,groundspeed=30)
        #vehicle.simple_goto(point2,airspeed=10)

    #if contador % 1537 == 0:
    #time.sleep(1)
    elif vehicle.battery.level < 80 and vehicle.battery.level >=60:
        vehicle.simple_goto(point1,groundspeed=30)
        #vehicle.simple_goto(point1,airspeed=10)
    else:
        vehicle.simple_goto(point3,groundspeed=30)
        #vehicle.simple_goto(point3,airspeed=10)
    #contador = contador + 1

print "Bateria esgotando, retornando para a posicao HOME"
vehicle.mode    = VehicleMode("RTL")

#Close vehicle object before exiting script
print "Encerrando veiculo"
vehicle.close()
