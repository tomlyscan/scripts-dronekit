print "Iniciando o simulador (SITL)"
from dronekit_sitl import SITL
import time
sitl = SITL()
sitl.download('copter','3.3',verbose=True)
sitl_args = ['-IO','--model','quad','-home=-35.363261,149.165230,584,353']
sitl.launch(sitl_args,await_ready=True,restart=True)

# importando do dronekit-python
from dronekit import connect, VehicleMode
import time

# conectando-se com o veiculo
# print "Conectando-se com o veiculo em: 'tcp:127.0.0.1:5760'"
print "Conectando-se com o veiculo em: '127.0.0.1:14550'"
vehicle = connect('127.0.0.1:14550',wait_ready=True)

# Obtencao de atributos do veiculo (estado)
print "Obtendo alguns valores de atributos do veiculo: "
print " GPS: %s" % vehicle.gps_0
print " Localizacao Global: %s" % vehicle.location.local_frame
print " Localizacao Global (altitude relativa): %s" % vehicle.location.global_relative_frame
print " Attitude: %s" %vehicle.attitude
print " Velocidade: %s" % vehicle.velocity
print " Gimbal status: %s" % vehicle.gimbal
print " Bateria: %s" % vehicle.battery
print " Ultima pulsacao: %s" % vehicle.last_heartbeat
print " EKF OK?: %s" % vehicle.ekf_ok
print " Rangefinder: %s" % vehicle.rangefinder
print " Rangefinder distance: %s" % vehicle.rangefinder.distance
print " Rangefinder voltage: %s" % vehicle.rangefinder.voltage
print " Heading: %s" % vehicle.heading
print " Is armable?: %s" % vehicle.is_armable
print " Status do sistema: %s" % vehicle.system_status.state
print " Mode: %s" % vehicle.mode.name
print " Groundspeed: %s" % vehicle.groundspeed
print " Airspeed: %s" % vehicle.airspeed
print " Armed: %s" % vehicle.armed

# Pegar a localizacao da 'Home' - sera 'None' na primeira vez ao setar o autopilot
while not vehicle.home_location:
    cmds = vehicle.commands
    cmds.download()
    cmds.wait_ready()
    if not vehicle.home_location:
        print " Waiting for home location... "
    # Da um print se ja possui uma home location
    print "\n Home location: %s" % vehicle.home_location

# Setar home location do veiculo, modo e mais alguns atributos (os setaveis)
print "\n Setar uma nova home location"
# A localizacao da home deve estar a km da home EKF, senao ocorrera falha
my_location_alt=vehicle.location.global_frame
my_location_alt.alt=222.0
vehicle.home_location=my_location_alt
print " Nova home location (do atributo - altitude pode ser 222): %s" % vehicle.home_location

# Confirmar o valor corrente do veiculo atraves de re-download dos comandos
cmds = vehicle.commands
cmds.download()
cmds.wait_ready()
print " Nova home location (do veiculo - altitude pode ser 222): %s" % vehicle.home_location

# Mudanca dos modos de voo
print "\nSet Vehicle.mode=GUIDED (atualmente: %s)" % vehicle.mode.name
vehicle.mode = VehicleMode("GUIDED")
while not vehicle.mode.name == 'GUIDED': # Esperar pela mudanca de modo de voo
    print " Esperando pela mudanca do modo de voo..."
    time.sleep(1)

# Verificar se o veiculo esta armado
while not vehicle.is_armable:
    print " Esperando a inicializacao do veiculo..."
    time.sleep(1)

print "\nSet Vehicle.armed=True (atualmente: %s)" % vehicle.armed
vehicle.armed = True
while not vehicle.armed:
    print " Esperando pelo armamento..."
    time.sleep(1)
print " Veiculo esta armado: %s" % vehicle.armed

# Adicionar, remover e atribuir callbacks
last_attitude_cache=None
def attitude_callback(self, attr_name, value):
    # 'attr_name' : atributo a ser observado (usado quando o callback possui multiplos atributos)
    # 'self' : objeto associado ao veiculo (usado se o callback eh diferente para multiplos veiculos)
    # 'value' : eh o valor atualizado do atributo
    global last_attitude_cache
    # publica somente quando ha mudancas no valor
    if value!=last_attitude_cache:
        print " CALLBACK: Attitude mudado para ", value
        last_attitude_cache=value

print "\nAdicionando atributo 'attitude' callback/observer em 'vehicle'"
vehicle.add_attribute_listener('attitude',attitude_callback)

print " Esperando 2seg para a invocacao do callback antes do observer ser removido"
time.sleep(2)

print " Removendo o observer do Vehicle.attitude"
# Removendo observer adicionando com 'add_attribute_listener() especificando o atributo e a funcao callback'
vehicle.remove_attribute_listener('attitude',attitude_callback)

# Adicionando callback de atribuicao de modo usando decorator (callbacks adicionados dessa forma nao podem ser removidos)
print "\nAdicionando atributo 'mode' callback/observer usando decorator"
@vehicle.on_attribute('mode')
def decorated_mode_callback(self, attr_name, value):
    # 'attr_name' eh um atributo observer (usado se o callback for utilizado  com multiplos atributos)
    # 'value' eh o valor atualizado do atributo
    print " CALLBACK: Modo mudado para", value

print " Set mode=STABILIZE (atualmente: %s) e esperando por callback" % vehicle.mode.name
vehicle.mode = VehicleMode("STABILIZE")

print " Esperando 2seg para invocar o callback antes de partir para o proximo exemplo"
time.sleep(2)

print "\n Tentativa de remover observer adicionando com 'on_attribute' decorator (pode falhar)"
try:
    vehicle.remove_attribute_listener('mode',decorated_mode_callback)
except:
    print " Excecao: Nao pode remover observer adicionado usando decorator"

# Demonstrando a captura do callback em uma mudanca de atributo
def wildcard_callback(self, attr_name, value):
    print " CALLBACK: (%s): %s" % (attr_name,value)

print "\nAdicionando um atributo ao callback detectando qualquer mudanca no atributo"
vehicle.add_attribute_listener('*',wildcard_callback)

print " Esperando 1seg para invocar o callback antes de remover o observer"
time.sleep(1)

print " Removendo o atributo observer do Vehicle"
# Removendo o observer adicionado com 'add_attribute_listener()'
vehicle.remove_attribute_listener('*',wildcard_callback)

# Obtendo/mudando (get/set) parametros do veiculo
print "\nLendo e escrevendo parametros"
print " Lendo o parametro 'THR_MIN' do veiculo: %s" % vehicle.parameters['THR_MIN']

print " Escrevendo o parametro 'THR_MIN' do veiculo: "
vehicle.parameters['THR_MIN']=10
print " Lendo o novo valor do parametro 'THR_MIN': %s" % vehicle.parameters['THR_MIN']

print "\nExibindo todos os parametros (iterando 'vehicle.parameters'):"
for key, value in vehicle.parameters.iteritems():
    print " Key:%s Value:%s" % (key,value)

print "\nCriando um parametro observer usando decorator"
# O parametro string eh case-insensitive
# Value eh cached (listeners sao atualizados na mudanca)
# Observer adicionado usando decorator sem poder remover

@vehicle.parameters.on_attribute('THR_MIN')
def decorated_thr_min_callback(self, attr_name, value):
    print " PARAMETRO DO CALLBACK: %s mudado para: %s" % (attr_name, value)

print "Escrevendo parametro do veiculo 'THR_MIN' : 20 (e esperando pelo callback)"
vehicle.parameters['THR_MIN']=20
for x in range(1,5):
    # Callbacks nao podem ser atualizados por alguns segundos
    if vehicle.parameters['THR_MIN']==20:
        break
    time.sleep(1)

# Funcao callback para "qualquer" parametro
print "\nCriando observer (removivel) para qualquer parametro usando uma string wildcard"
def any_parameter_callback(self, attr_name, value):
    print " QUALQUER PARAMETRO DO CALLBACK: %s alterado para: %s" % (attr_name, value)

# Adicionando um observer aos parametros do vehicle usando uma string wildcard
vehicle.parameters.add_attribute_listener('*', any_parameter_callback)
print " Alterando os parametros THR_MID e THR_MIN (e esperando pelo callback)"
vehicle.parameters['THR_MID']=400
vehicle.parameters['THR_MIN']=30

# Resetando as variaveis a valores sensiveis
print "\nResetando os parametros/atributos do veiculo e finalizando"
vehicle.mode = VehicleMode("STABILIZE")
vehicle.armed=False
vehicle.parameters['THR_MIN']=130
vehicle.parameters['THR_MID']=500


# Encerrar o objeto veiculo antes de finalizar o script
vehicle.close()

# Desligar o simulador
# sitl.stop() # esse comando resulta em erros no interpretador python
print("Terminado")
