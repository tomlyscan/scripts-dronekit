print "Iniciando o simulador (SITL)"
from dronekit_sitl import SITL
import time
sitl = SITL()
sitl.download('copter','3.3',verbose=True)
sitl_args = ['-IO','--model','quad','-home=-35.363261,149.165230,584,353']
sitl.launch(sitl_args,await_ready=True,restart=True)

# importando do dronekit-python
from dronekit import connect, VehicleMode
import time

# conectando-se com o veiculo
print "Conectando-se com o veiculo em: 'tcp:127.0.0.1:5760'"
vehicle = connect('tcp:127.0.0.1:5760',wait_ready=True)

while(True):
    # Obtencao de atributos do veiculo (estado)
    print "Obtendo alguns valores de atributos do veiculo: "
    print " GPS: %s" % vehicle.gps_0
    print " Localizacao Global: %s" % vehicle.location.local_frame
    print " Localizacao Global (altitude relativa): %s" % vehicle.location.global_relative_frame
    print " Attitude: %s" %vehicle.attitude
    print " Velocidade: %s" % vehicle.velocity
    print " Gimbal status: %s" % vehicle.gimbal
    print " Bateria: %s" % vehicle.battery
    print " Ultima pulsacao: %s" % vehicle.last_heartbeat
    print " EKF OK?: %s" % vehicle.ekf_ok
    print " Rangefinder: %s" % vehicle.rangefinder
    print " Rangefinder distance: %s" % vehicle.rangefinder.distance
    print " Rangefinder voltage: %s" % vehicle.rangefinder.voltage
    print " Heading: %s" % vehicle.heading
    print " Is armable?: %s" % vehicle.is_armable
    print " Status do sistema: %s" % vehicle.system_status.state
    print " Mode: %s" % vehicle.mode.name
    print " Groundspeed: %s" % vehicle.groundspeed
    print " Airspeed: %s" % vehicle.airspeed
    print " Armed: %s" % vehicle.armed
    time.sleep(1)

# Encerrar o objeto veiculo antes de finalizar o script
vehicle.close()

# Desligar o simulador
# sitl.stop() # esse comando resulta em erros no interpretador python
print("Terminado")
